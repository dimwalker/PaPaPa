﻿using PaPaPa.Data.EF;
using PaPaPa.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Core.Base
{
    public class BaseCore<TEntity, TPrimaryKey>
        where TEntity : EntityBase<TPrimaryKey>, new()
    {
        public async static Task Create(TEntity datingInfo)
        {
            using (var ef = new DataWrapper<TEntity>())
            {
                ef.Add(datingInfo);
                await ef.SaveAsync();
            }
        }
        public async static Task Modify(TEntity datingInfo)
        {
            using (var ef = new DataWrapper<TEntity>())
            {
                ef.Modify(datingInfo);
                await ef.SaveAsync();
            }
        }

        public async static Task<TEntity> FindById(TPrimaryKey id)
        {
            using (var ef = new DataWrapper<TEntity>())
            {
                return await ef.FindAsync(x => x.Id.Equals(id));
            }
        }

        public async static Task<List<TEntity>> FindListByCondition(Expression<Func<TEntity, bool>> conditionExp)
        {
            using (var ef = new DataWrapper<TEntity>())
            {
                return await ef.FindAllAsync(conditionExp);
            }
        }
        public async static Task<List<TEntity>> FindAllList()
        {
            using (var ef = new DataWrapper<TEntity>())
            {
                return await ef.FindAllAsync();
            }
        }
    }
}
