﻿using Framework.Common.EnumOperation;
using Framework.Common.ExceptionOperation;
using PaPaPa.Core.Base;
using PaPaPa.Data.EF;
using PaPaPa.Models.BasicData;
using PaPaPa.Models.Datings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Core.Datings
{
    public class DatingApplicationCore : BaseCore<DataApplication, int>
    {
        public async static Task<DatingInfo> FindById(int id)
        {
            DatingInfo dating;

            using (var ef = new DataWrapper<DatingInfo>())
            {
                dating = await ef.FindAsync(x => x.Id == id);
                if (dating == null)
                {
                    throw new GException(EnumHelper.GetEnumDescription(ErrorCode.DatingIdIsNoExists).Description);
                }
                return dating;
            }
        }
    }
}
