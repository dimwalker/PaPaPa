﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaPaPa.Data
{
    /// <summary>
    /// Url常量
    /// </summary>
    public class UrlConstant
    {
        /// <summary>
        /// 主页
        /// </summary>
        public static readonly object HOME = new { Action = "Index", Controller = "Home", area = "Home" };
    }
}