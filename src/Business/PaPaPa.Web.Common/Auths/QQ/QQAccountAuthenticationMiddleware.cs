﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Infrastructure;
using Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace PaPaPa.Web.Common.Auths.QQ
{
    public class QQAccountAuthenticationMiddleware : AuthenticationMiddleware<QQAccountAuthenticationOptions>
    {
        private readonly HttpClient _httpClient;

        public QQAccountAuthenticationMiddleware(OwinMiddleware next, IAppBuilder app, QQAccountAuthenticationOptions options)
            : base(next, options)
        {
            if (string.IsNullOrWhiteSpace(base.Options.AppId))
            {
                throw new ArgumentException("AppId");
            }
            if (string.IsNullOrWhiteSpace(base.Options.AppSecret))
            {
                throw new ArgumentException("AppSecret");
            }

            this._httpClient = new HttpClient();
            this._httpClient.Timeout = base.Options.BackchannelTimeout;
            this._httpClient.MaxResponseContentBufferSize = 0xa00000L;
        }

        protected override AuthenticationHandler<QQAccountAuthenticationOptions> CreateHandler()
        {
            return (AuthenticationHandler<QQAccountAuthenticationOptions>)new QQAccountAuthenticationHandler(this._httpClient);
        }
    }
}
