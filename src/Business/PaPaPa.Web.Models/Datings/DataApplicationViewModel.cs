﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Models.Datings
{
    public class DataApplicationViewModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 约会Id
        /// </summary>
        [Display(Name = "约会Id")]
        public int DatingId { get; set; }
    }
}
