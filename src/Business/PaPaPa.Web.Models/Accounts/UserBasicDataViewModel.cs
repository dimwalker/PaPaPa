﻿using Framework.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Models.Accounts
{
    public class UserBasicDataViewModel
    {
        [MapperIgnore]
        public int Id { get; set; }

        [MapperIgnore]
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        [Display(Name = "头像")]
        public string ImgSrc { get; set; }

        [Display(Name = "昵称")]
        public string NickName { get; set; }

        [Display(Name = "邮箱")]
        public string Email { get; set; }

        [Display(Name = "手机号码")]
        public string MobilePhone { get; set; }

        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "修改时间")]
        public DateTime ModifyTime { get; set; }

        [Display(Name = "省份")]
        public string Province { get; set; }

        [Display(Name = "城市")]
        public string City { get; set; }

        [Display(Name = "区域")]
        public string Area { get; set; }
    }
}
